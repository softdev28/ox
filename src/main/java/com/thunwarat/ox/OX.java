/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thunwarat.ox;

import java.util.Scanner;

/**
 *
 * @author ACER
 */
public class OX {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int row;
        int col;
        char board[][] = new char[3][3];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                board[i][j] = '-';
            }
        }
        System.out.println("Welcome to OX Game");
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(board[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println("Turn O");
        System.out.println("Please input row, col:");
        row = kb.nextInt() - 1;
        col = kb.nextInt() - 1;
        char ch = 'O';
        board[row][col] = ch;
        System.out.println();
        int count = 0;
        while (true) {
            count++;
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    System.out.print(board[i][j] + " ");
                }
                System.out.println();
            }
            //แนวนอน
            if(board[0][0] != '-' && board[0][0] == board[0][1] && board[0][1] == board[0][2]){
                System.out.println(">>>" + ch + " Win<<<");
                break;
            }
            if(board[1][0] != '-' && board[1][0] == board[1][1] && board[1][1] == board[1][2]){
                System.out.println(">>>" + ch + " Win<<<");
                break;
            }
            if(board[2][0] != '-' && board[2][0] == board[2][1] && board[2][1] == board[2][2]){
                System.out.println(">>>" + ch + " Win<<<");
                break;
            }
            //แนวตั้ง
            if(board[0][0] != '-' && board[0][0] == board[1][0] && board[1][0] == board[2][0]){
                System.out.println(">>>" + ch + " Win<<<");
                break;
            }
            if(board[0][1] != '-' && board[0][1] == board[1][1] && board[1][1] == board[2][1]){
                System.out.println(">>>" + ch + " Win<<<");
                break;
            }
            if(board[0][2] != '-' && board[0][2] == board[1][2] && board[1][2] == board[2][2]){
                System.out.println(">>>" + ch + " Win<<<");
                break;
            }
            //แนวทแยง
            if(board[0][0] != '-' && board[0][0] == board[1][1] && board[1][1] == board[2][2]){
                System.out.println(">>>" + ch + " Win<<<");
                break;
            }
            if(board[0][2] != '-' && board[0][2] == board[1][1] && board[1][1] == board[2][0]){
                System.out.println(">>>" + ch + " Win<<<");
                break;
            }
            if(count==9){
                System.out.println(">>>Draw<<<");
                break;
            }
            
            if (count % 2 == 0) {
                ch = 'O';
            } else {
                ch = 'X';
            }

            System.out.println("Turn " + ch);
            System.out.println("Please input row, col:");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
            board[row][col] = ch;
        }
    }

}
